import Vue from 'vue'
import VueResource from 'vue-resource'
import $ from 'jquery'

import HeaderStatic from './components/header/header.vue';
import FooterStatic from './components/footer/footer.vue';
import MainVideo from './components/main/video/video.vue';
import MainParrilla from './components/main/parrilla/parrilla.vue';
import MainSurvey from './components/main/survey/survey.vue';

Vue.use(VueResource);

new Vue({
  el: '#app',
  components: {
    HeaderStatic,
    MainVideo,
    MainParrilla,
    MainSurvey,
    FooterStatic
  },

  mounted: function () {
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };
  }
});
