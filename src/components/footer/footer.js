import $ from 'jquery';
export default {
  name: 'footer-static',
  data: function () {
    return {
      mobile: $(window).width() < 758,
      open: false,
    }
  },
  methods: {
    openFooter: function (){
      this.open = !this.open;
    }
  }
}
