import $ from 'jquery'

export default {
  name: 'header-static',
  data: function () {
    return {
      mobile: $(window).width() < 768
    }
  },
  mounted: function(){

    var mapping = googletag.sizeMapping()
                  .addSize([320, 400], [[300, 50], [320, 50]])
                  .addSize([320, 480], [[300, 50], [320, 50]])
                  .addSize([1050, 200], [[1000, 40], [1300, 730], [1300, 600]])
                  .addSize([750, 200], [[320, 50], [300, 50]])
                  .build();

    window.addEventListener("load", function(){
      googletag.cmd.push(function() {
        googletag.defineSlot('/100249493/golcaracol', [[1000, 40], [1300, 730], [1000, 90], [1300, 600], [320, 50], [300, 50]], 'div-gpt-ad-1506528348649-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
        // googletag.defineSizeMapping(mapping);
      });

      googletag.cmd.push(function() { googletag.display('div-gpt-ad-1506528348649-0'); });

      googletag.pubads().addEventListener("slotRenderEnded", function(event) {
        if(!event.isEmpty){
          if (event.size[0] > 800 && event.size[1] > 500) {
            var el = document.getElementById("main");
            if(el) {
              el.className += el.className ? ' dfp-toma' : 'dfp-toma';
            }
          }
        }
      });

      // $(window).scroll(function () {
      //   if ($(this).scrollTop() > 0) {
      //   $('#header').addClass('fixed_menu');
      //   } else {
      //     $('#header').removeClass('fixed_menu');
      //   }
      // });

      if($(window).width() < 768){
        $("#header .header-first .social").click(function() {
          $(this).toggleClass("active");
        });
      }
      $(".group-wrapper2, .header-second").click(function () {
        var _el = $(".group-wrapper2");
        var _screen = $(window).width();
        if (!$(".menu").hasClass("active_menu")) {
          $(".menu").addClass("active_menu");
          $(".header-second").show();
          _el.addClass("active_click");
          if (_screen < 768) {
            $(".block.block-icck-social-networks").addClass("active_mobile");
            $(".field-name-field-logo > img").hide();
            $(".header-first section.block-block-47").eq(0).addClass("activeMenu");
            $("header#header").addClass("active_menu");
            $("body").addClass("show_menu");
            $(".bloque-1").hide();
            $(".block-block-47 .field-type-link-field").hide();
            $(".block-block-47 .group-logo-copa").hide();
          }
        } else {
          $(".menu").removeClass("active_menu");
          _el.removeClass("active_click");
          $(".header-second").hide();
          if (_screen < 768) {
            $(".block.block-icck-social-networks").removeClass("active_mobile");
            $(".field-name-field-logo > img").fadeIn();
            $(".header-first section.block-block-47").eq(0).removeClass("activeMenu");
            $("header#header").removeClass("active_menu");
            $("body").removeClass("show_menu");
            $(".block-block-47 .field-type-link-field").show();
            $(".block-block-47 .group-logo-copa").show();
            $(".bloque-1").show();
          }
        }
      });
    }, false);

  }
};
