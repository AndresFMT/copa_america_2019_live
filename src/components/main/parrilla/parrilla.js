import $ from 'jquery';
import 'slick-carousel';

export default {
  name: 'main-parrilla',
  data: function () {
    return {
      contentView: [],
      parrilla: window.parrilla
    };
  },
  mounted: function(){
  	var _this = this;
    _this.parrilla = "";

    function reset() {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200 && this.responseText.length > 0) {
          window.parrilla = this.responseText;
          var regex = /<body>(.|\s)*?<\/body>/gm;
          window.parrilla = regex.exec(window.parrilla)[0];
          window.parrilla = window.parrilla.split("<body>").join("").split("</body>").join("").split('style="display:none;visibility:hidden;').join("");
          _this.parrilla = window.parrilla;
          var x = /data-cfsrc/g;
          _this.parrilla = _this.parrilla.replace(x ,"src");
          _this.parrilla = _this.parrilla.replace('<style type="text/css">', '<div class="none">');
          _this.parrilla = _this.parrilla.replace('</style>', '</div>');
        }
      };
      xhttp.open("GET", window.url2, true);
      xhttp.send();
    }

    reset();
    setInterval(() => {
      reset();
    },100000);
  }
};
