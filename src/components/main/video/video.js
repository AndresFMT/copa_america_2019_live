import $ from 'jquery';

export default {
  name: 'main-video',
  data: function () {
    return {
      mobile: $(window).width() < 758,
      iframe: "https://mdstrm.com/live-stream/58d18bd236e5f69e5887332b",
      play: "https://eventos.caracolplay.com/eliminatorias.html",
      video: window.htmlVideo
    };
  },
  mounted: function(){
    var _this = this;
    var size = [728, 90];
    if( this.mobile ){
      size = [320, 50];
    }

    if( !this.mobile ){
      $(window).on('scroll', function(event) {
        var _el = $(".video");
        if( $(this).scrollTop() > _el.offset().top ){
          if( !_el.hasClass('scroll') ){
            _el.addClass('scroll');
          }
        }else{
          _el.removeClass('scroll');
        }
      });
    }
    if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
      $('body').addClass('iphone');
      if(/iPad/.test(navigator.userAgent) && !window.MSStream){
        $('body').addClass('ipad');
      }
    }
    window.addEventListener("load", function(){
      $(".public-list").slick({
        responsive: [
         {
          breakpoint: 3000,
          settings:{
            slidesToShow: 7,
            slidesToScroll: 3,
            infinite: true,
            dots: false,
            centerPadding: '30px',
            autoplay: true,
            adaptiveHeight: true,
            autoplaySpeed: 3000
          }
         },
         {
          breakpoint: 768,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 2,
            infinite: true,
            dots: false,
            //centerMode: true,
            centerPadding: '10px',
            autoplay: true,
            autoplaySpeed: 3000
          }
         },
         {
          breakpoint: 321,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: false,
            centerMode: true,
            centerPadding: '10px',
            autoplay: true,
            autoplaySpeed: 3000
          }
         }
        ]
      });
    }, false);

    // var xhttp = new XMLHttpRequest()
    // xhttp.onreadystatechange = function() {
    //     if (this.readyState == 4 && this.status == 200 && this.responseText.length > 0) {
    //       window.json = JSON.parse(this.responseText);

    //       window.htmlVideo = window.json.body['#markup'];
    //       window.htmlVideo = window.htmlVideo.split("<p>").join("").split("</p>").join("");
    //       _this.video = window.htmlVideo;

    //     }
    // };
    // xhttp.open("GET", window.url1, true);
    // xhttp.send();
    // xhttp.onreadystatechange = function() {
    //     if (this.readyState == 4 && this.status == 200 && this.responseText.length > 0) {
    //       window.htmlVideo = this.responseText;
    //       var regex = /<body>(.|\s)*?<\/body>/gm;
    //       window.htmlVideo = regex.exec(window.htmlVideo)[0];
    //       window.htmlVideo = window.htmlVideo.split("<body>").join("").split("</body>").join("");
    //       _this.htmlVideo = window.htmlVideo
    //     }
    // };
    // xhttp.open("GET", window.url1, true);
    // xhttp.send();

  }
}
